public class TryCatchExceptions {

    public static void main(String [] args) {

            int [] numbers = { 1 , 2 , 3} ;

            try {
                System.out.println(numbers[4]);
            } catch (Exception e) {
                System.out.println("This is why QA Engineers always have to do boundary testing! The array only has 3 " +
                        "and you've requested 4! ");
            }
        }
}



